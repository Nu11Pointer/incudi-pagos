<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
Use App\Cuota;
use Illuminate\Support\Facades\DB;

class Concurrente extends Model
{
	protected $table = 'concurrente';
	protected $primaryKey = 'dni';

	protected $appends = array('moroso');

    /*
		Se debe revisar si algún concurrente a cargo tiene alguna cuota adeudada
    */
    public function getMorosoAttribute(){
		$adeudadas = DB::select("
					SELECT subquery.n_recibo, subquery.vencimiento 
						FROM (SELECT n_recibo, id_concurrente, DATE_ADD(cuota.fecha, INTERVAL 1 MONTH) AS vencimiento 
								FROM cuota 
									WHERE n_recibo is null AND id_concurrente=".$this->dni.") as subquery
							WHERE subquery.vencimiento < DATE(NOW())");

        return !empty($adeudadas); //Si no está vacia, debe cuotas, por lo que es MOROSOOOO
    }
}
