<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cuota extends Model
{
    protected $table = 'cuota';
    protected $primaryKey = 'id_cuota';

    protected $appends = array('vencida','fecha_human');

    public function getVencidaAttribute(){
		$fecha_vencimiento = strtotime('+1 month',strtotime($this->fecha));
		//Si es menor, la fecha ya pasó, la cuota venció
		return ($this->n_recibo==null && $fecha_vencimiento < time());
    }

    public function getFechaHumanAttribute(){
    	setlocale(LC_TIME,"Spanish","es_RA");
    	$fecha_letras = \Carbon\Carbon::createFromTimestamp(strtotime($this->fecha))->formatLocalized('%B del %Y');
    	return $fecha_letras;
    }
}
