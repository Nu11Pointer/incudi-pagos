<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
Use App\Concurrente;

class Socio extends Model
{
    protected $table = 'socio';
    protected $primaryKey = 'dni';

    protected $appends = array('moroso');

    /*
		Se debe revisar si algún concurrente a cargo tiene alguna cuota adeudada
    */
    public function getMorosoAttribute()
    {
        $concurrentes = Concurrente::where('a_cargo',$this->dni)->get();
        foreach($concurrentes as $concurrente)
        	if($concurrente->moroso)
        		return true;
        //Si ningun concurrente es moroso, entonces el socio tampoco lo es
        return false;
    }
}