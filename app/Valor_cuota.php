<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Valor_cuota extends Model
{
    protected $table = 'valor_cuota';
    protected $primaryKey = 'servicio';

    public $incrementing = false;
	public $timestamps = false;
}
