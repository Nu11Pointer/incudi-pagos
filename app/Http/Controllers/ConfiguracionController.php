<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ConfiguracionController extends Controller
{
    public function dbdump(){
        $ds = DIRECTORY_SEPARATOR;
        // $schema = $this->argument('schema');
        // $password = $this->argument('password');
        $path = database_path() . $ds . 'backups' . $ds . date('Y') . $ds . date('m') . $ds;
        $file = date('Y-m-d') . '_mysqldump.sql';

        // $command = sprintf('mysqldump %s -u forge -p\'%s\' > %s', $schema, $password, $path . $file);
        $command = sprintf('mysqldump %s -u root > %s', 'incudi_pagos', $path . $file);
        
        if (!is_dir($path)) {
            mkdir($path, 0755, true);
        }
        exec($command,$output, $return_var);

        if($return_var == 0)
            return response()->download($path . $file);

    }
}
