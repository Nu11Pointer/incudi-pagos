<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Socio;
Use App\Concurrente;
Use App\Cuota;
Use App\Pago;
use PDF;

class SocioController extends Controller
{
    //GET /socio
    public function index(){
      return Socio::all();
    }
    //POST /socio
    public function store(Request $request){
      $socio = new Socio();

      $socio -> dni = $request -> dni;
      $socio -> nombre = $request -> nombre;
      $socio -> apellido = $request -> apellido;
      $socio -> domicilio = $request -> domicilio;
      $socio -> mail = $request -> mail;
      $socio -> telefono = $request -> telefono;

      $socio -> save();

      // return redirect('/');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $dni
     * @return \Illuminate\Http\Response
     * DELETE /socio/{dni}
     */
    public function destroy($dni){
      Socio::where('dni',$dni)->delete();
      return 204;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * PUT/PATCH /socio/{dni}
     */
    public function update(Request $request, $dni)
    {
        $socio= Socio::find($dni);
        
        $socio->dni = $request->get('dni'); //Cambian la key?
        $socio->nombre = $request->get('nombre');
        $socio->apellido = $request->get('apellido');
        $socio->domicilio = $request->get('domicilio');
        $socio->mail = $request->get('mail');
        $socio->telefono = $request->get('telefono');
        $socio->domicilio = $request->get('domicilio');

        $socio->save();
    }

    //GET /socio/{dni}
    public function show($dni)
    {
        $socio = Socio::find($dni);
        
        //Concurrentes del socio
        $concurrentes = Concurrente::where('a_cargo',$dni)->get();
        $socio->concurrentes = $concurrentes;

        //Pagos del socio
        $pagos = Pago::where('id_socio',$dni)->get();
        $socio->pagos = $pagos;

        return $socio;
    }

    public function cuotasImpagas($dni){
        $concurrentes = Concurrente::where('a_cargo',$dni)->get();
        $cuotas_pendientes = [];
        foreach($concurrentes as $concurrente){
            $nombre_apellido = $concurrente->nombre.' '.$concurrente->apellido;
            $cuotas_concurrente = Cuota::where('id_concurrente', $concurrente->dni)->where('n_recibo',null)->orderBy('fecha','ASC')->get();
            foreach($cuotas_concurrente as $cuota){
                $cuota['nombre_concurrente']=$nombre_apellido;
                array_push($cuotas_pendientes,$cuota);
            }
        }
        return $cuotas_pendientes;
    }

    public function sociosMorosos(){
      $socios = Socio::all();
      $morosos = [];
      foreach($socios as $socio)
        if($socio->moroso)
          array_push($morosos,$socio);
      return $morosos;
    }

    public function sociosMorososPDF(){
      // $socios = Socio::all();
      $socios = Socio::orderBy('apellido')->get();
      $morosos = [];
      foreach($socios as $socio)
        if($socio->moroso)
          array_push($morosos,$socio);

      $timezone  = -3; //(GMT -3:00)
      $hoy = gmdate("Y/m/j H:i:s", time() + 3600*($timezone+date("I"))); 

      setlocale(LC_TIME,"Spanish","es_RA");
      $fecha_hora = \Carbon\Carbon::createFromTimestamp(strtotime($hoy))->formatLocalized('%A %d de %B de %Y a las %H:%m');
      $fecha_hora = utf8_encode($fecha_hora);

      $pdf = PDF::loadView('morososPDF', ['morosos'=> $morosos, 'fecha_hora'=> $fecha_hora]);
      return $pdf->stream();
    }
}
