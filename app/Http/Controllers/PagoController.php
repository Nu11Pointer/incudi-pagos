<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Pago;
Use App\Cuota;
use NumeroALetras;
use PDF;
use Illuminate\Support\Facades\Response;

class PagoController extends Controller
{
    public function pagar(Request $request, $dni){
    	//decodificar cuotas
    	$id_cuotas = $request->cuotas_pagar;
    	//Revisar que el importe alcance
    	if(! $this->alcanzaImporte($request->importe, $id_cuotas))
    		return response('El importe es insuficiente', 400)
                  ->header('Content-Type', 'text/plain');

    	$pago = new Pago();

    	$pago -> id_socio = $request -> id_socio;
    	$pago -> pagador = $request -> pagador;
    	$pago -> importe = $request -> importe;
    	$pago -> fecha_pago = $request -> fecha_pago;
    	$pago -> concepto = $request -> concepto;

    	$pago->save();

    	//Hay que dar como pagas las cuotas
    	foreach($id_cuotas as $id_cuota){
    		$cuota= Cuota::find($id_cuota);
    		$cuota->n_recibo = $pago->n_recibo;
    		$cuota->save();
    	}

    	return Response::json(array('success' => true, 'n_recibo' => $pago->n_recibo), 200);
    }

    public function eliminarPago($n_recibo){
      Pago::where('n_recibo',$n_recibo)->delete();
      return 204;
    }

    /*
    	*Aca se revisa si la plata de pago alcanza o no
    */
    private function alcanzaImporte($importe, $id_cuotas){
        if(empty($id_cuotas))
            return true;
    	else return (Cuota::whereIn('id_cuota',$id_cuotas)->sum('importe') <= $importe);
    }

    public function generarPDF($n_recibo){
		$pago = Pago::find($n_recibo);
    	$precio_letras = NumeroALetras::convertir($pago->importe,'pesos','centavos');

    	setlocale(LC_TIME,"Spanish","es_RA");
    	$fecha_letras = \Carbon\Carbon::createFromTimestamp(strtotime($pago->fecha_pago))->formatLocalized('%A %d de %B de %Y');

    	//Para quitar el b de binary que produce el cambio a español
    	$fecha_letras = utf8_encode($fecha_letras);

		$pdf = PDF::loadView('recibo', ['pago'=> $pago, 'precio_letras'=> $precio_letras, 'fecha_letras'=>$fecha_letras]);
		//return $pdf->download('recibo.pdf');
		return $pdf->stream();
	}
}
