<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Concurrente;
Use App\Socio;
Use App\Cuota;

class ConcurrenteController extends Controller
{
  //GET /concurrente
	public function index(){
      return Concurrente::all();
    }

    //POST /concurrente
    public function store(Request $request){
      $concurrente = new Concurrente();

      $concurrente -> dni = $request -> dni;
      $concurrente -> a_cargo = $request -> a_cargo;
      $concurrente -> nombre = $request -> nombre;
      $concurrente -> apellido = $request -> apellido;
      $concurrente -> fecha_ingreso = $request -> fecha_ingreso;
      $concurrente -> obra_social = $request -> obra_social;
      $concurrente -> servicio = $request -> servicio;

      $concurrente -> save();

      return 200;//OK
    }
    /*
      * DELETE /concurrente/{dni}
     */
    public function destroy($dni){
      Concurrente::find($dni)->delete();
      return 204;
    }

    //GET /concurrente/{dni}
    public function show($dni)
    {
        $concurrente = Concurrente::find($dni);

        $socio_a_cargo = Socio::find($concurrente -> a_cargo);
        $concurrente->a_cargo_completo = $socio_a_cargo;

        $cuotas = Cuota::where('id_concurrente',$dni)->orderBy('n_recibo','ASC')->orderBy('fecha', 'ASC')->get();

        $concurrente->cuotas = $cuotas;

        return $concurrente;
    }
    /*
     * PUT/PATCH /concurrente/{dni}
     */
    public function update(Request $request, $dni)
    {
        $concurrente= Concurrente::find($dni);
        
        $concurrente->dni = $request->get('dni'); //Cambian la key?
        $concurrente->a_cargo = $request->get('a_cargo');
        $concurrente->nombre = $request->get('nombre');
        $concurrente->apellido = $request->get('apellido');
        $concurrente->fecha_ingreso = $request->get('fecha_ingreso');
        $concurrente->obra_social = $request->get('obra_social');
        $concurrente->servicio = $request->get('servicio');

        $concurrente->save();
    }
}
