<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Cuota;
Use App\Valor_cuota;
Use App\Concurrente;

class CuotaController extends Controller
{
    //POST /cuota
    public function store(Request $request){
      $cuota = new Cuota();

      $cuota -> id_concurrente = $request -> id_concurrente;
      $cuota -> fecha = CuotaController::armarFecha($request -> anio, $request -> mes);
      $cuota -> importe = $request -> importe;

      $cuota -> save();
      return 200;
    }

    //POST /cuota/all
    public function storeAll(Request $request){

      $concurrentes = Concurrente::all();
      $importes = Valor_cuota::all();
      $importes = $importes->pluck('valor','servicio'); //"centro de día 1" => 5985.0 , ...

      foreach($concurrentes as $concurrente){
        $cuota = new Cuota();
        $cuota -> id_concurrente = $concurrente -> dni;
        $cuota -> fecha = CuotaController::armarFecha($request -> anio, $request -> mes);
        $cuota -> importe = $importes[$concurrente->servicio];
        $cuota -> save();
      }

      return 200;
    }

    private static function armarFecha($anio,$mes){
        $meses = ["enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre"];
        $nro_mes = array_search($mes, $meses)+1;
        if($nro_mes<10) //Una sola cifra
          $nro_mes="0".$nro_mes;
        return ($anio)."-".$nro_mes."-01";
    }

    //DELETE /cuota/{id_cuota}
    public function destroy($id_cuota){
      Cuota::find($id_cuota)->delete();
      return 204; //Empty
    }

    //POST /cuota/valor
    public function actualizarValor(Request $request){
      $valor_cuota = Valor_cuota::find($request->servicio);
      $valor_cuota->valor = $request->nuevo_valor;
      $valor_cuota->save();
      return 200;
    }

    //GET /cuota/valor
    public function obtenerValor(){
      return Valor_cuota::all();
    }
}
 