-- DROP DATABASE IF EXISTS incudi_pagos;
-- CREATE DATABASE incudi_pagos;

INSERT INTO socio (dni, nombre, apellido, domicilio, mail, telefono, created_at, updated_at) VALUES (123456789, 'Mirtha', 'Legrand', 'Buenos Aires 123', 'mirtha@legrand.com.ar', 4564064, now(), now());
INSERT INTO socio (dni, nombre, apellido, domicilio, mail, telefono, created_at, updated_at) VALUES (987654321, 'Marcelo', 'Tinelli', 'Bolivar 0', 'marcelo@videmoatch.com', 9999, now(), now());
INSERT INTO socio (dni, nombre, apellido, domicilio, mail, telefono, created_at, updated_at) VALUES (555, 'Susana', 'Gimenez', 'Telefe 10', 'su@gimenez.com', 4158, now(), now());

INSERT INTO concurrente (dni, a_cargo, nombre, apellido, fecha_ingreso, obra_social, servicio, created_at, updated_at) VALUES (100, 123456789, 'Michael', 'De Santa', '2018-08-08', 'OMINT', 'hogar',now(), now());
INSERT INTO concurrente (dni, a_cargo, nombre, apellido, fecha_ingreso, obra_social, servicio, created_at, updated_at) VALUES (101, 987654321, 'Franklin', 'Clinton', '2018-08-10', 'Osde', 'centro de día 1',now(), now());
INSERT INTO concurrente (dni, a_cargo, nombre, apellido, fecha_ingreso, obra_social, servicio, created_at, updated_at) VALUES (102, 555, 'Trevor', 'Philips', '2018-08-11', 'OMINT', 'centro de día 2',now(), now());

INSERT INTO cuota (id_concurrente, fecha, importe) VALUES (100, '2018-07-01', 1000);
INSERT INTO cuota (id_concurrente, fecha, importe) VALUES (100, '2018-08-01', 1000);
INSERT INTO cuota (id_concurrente, fecha, importe) VALUES (100, '2018-09-01', 1000);
INSERT INTO cuota (id_concurrente, fecha, importe) VALUES (100, '2018-10-01', 1000);
INSERT INTO cuota (id_concurrente, fecha, importe) VALUES (100, '2018-11-01', 1000);
INSERT INTO cuota (id_concurrente, fecha, importe) VALUES (100, '2018-12-01', 1000);
INSERT INTO cuota (id_concurrente, fecha, importe) VALUES (100, '2019-1-01', 1000);
INSERT INTO cuota (id_concurrente, fecha, importe) VALUES (100, '2019-12-01', 1000);

INSERT INTO pago (id_socio, pagador, importe, fecha_pago, concepto) VALUES (555, 'Susana Gimenez', '10000', now(), 'Cuotas de fulano');

-- mysqldump --user="root" --password="" --databases incudi_pagos --result-file=dump.sql