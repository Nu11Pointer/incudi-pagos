<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateValorCuotaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('valor_cuota', function (Blueprint $table) {
            $table->string('servicio');
            $table->primary('servicio');
            $table->float('valor', 10, 2);
        });

        // Insert some stuff
        DB::table('valor_cuota')->insert([
                [
                'servicio' => 'centro de día 1',
                'valor' => 0
                ],
                [
                'servicio' => 'centro de día 2',
                'valor' => 0
                ],
                [
                'servicio' => 'residencia',
                'valor' => 0
                ],
                [
                'servicio' => 'hogar',
                'valor' => 0
                ],
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('valor_cuota');
    }
}
