<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pago', function (Blueprint $table) {
            $table->increments('n_recibo');
            $table->integer('id_socio');
            $table->foreign('id_socio')->references('dni')->on('socio')->onDelete('cascade');
            $table->string('pagador');
            $table->float('importe', 8, 2);
            $table->date('fecha_pago');
            $table->string('concepto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pago');
    }
}
