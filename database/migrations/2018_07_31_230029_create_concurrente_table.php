<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConcurrenteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('concurrente', function (Blueprint $table) {
            $table->integer('dni');
            $table->primary('dni');
            $table->integer('a_cargo');
            $table->foreign('a_cargo')->references('dni')->on('socio');
            $table->string('nombre');
            $table->string('apellido');
            $table->date('fecha_ingreso');
            $table->string('obra_social');
            $table->enum('servicio', ['centro de día 1', 'centro de día 2', 'residencia', 'hogar']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('concurrente');
    }
}
