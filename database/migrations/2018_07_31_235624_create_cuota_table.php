<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuotaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuota', function (Blueprint $table) {
            $table->increments('id_cuota');
            //$table->primary('cod_cuota'); Se hace solo
            $table->integer('n_recibo')->nullable()->unsigned()->default(null);
            $table->foreign('n_recibo')->references('n_recibo')->on('pago')->onDelete('set null');
            $table->integer('id_concurrente');
            $table->foreign('id_concurrente')->references('dni')->on('concurrente')->onDelete('cascade');
            $table->date('fecha');
            $table->float('importe', 10, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuota');
    }
}
