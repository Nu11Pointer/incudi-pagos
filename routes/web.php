<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Use App\Socio;
Use App\Concurrente;
Use App\Pago;

Route::get('/', function () {
    return redirect('vistaSocios');
});

Route::get('/vistaSocios', function () {
    return view('socios');
});

Route::get('/vistaSocios/{dni}', function ($dni) {
	//$socio = Socio::where('dni',$dni)->first();
    return view('socio_detalles', ['dni' => $dni]);
});

Route::get('/vistaConcurrentes', function () {
    return view('concurrentes');
});

Route::get('/vistaConcurrentes/{dni}', function ($dni) {
    return view('concurrente_detalles', ['dni_concurrente' => $dni]);
});

Route::get('/pago/{dni_socio}', function ($dni_socio) {
    return view('pago', ['dni_socio' => $dni_socio]);
});

Route::post('/pago/{dni_socio}','PagoController@pagar');
Route::delete('/pago/{n_recibo}','PagoController@eliminarPago');

Route::get('/pago/recibo/{n_recibo}', 'PagoController@generarPDF');

Route::get('/pagobeta/recibo/{n_recibo}', function($n_recibo){
	$pago = Pago::find($n_recibo);
	//$pdf = PDF::loadView('recibo', ['pago'=> $pago]);
	return view('recibo', ['pago'=> $pago]);
});

// Route::get('/socios', function () {
//     return Socio::all();
// });

// Route::post('/createSocio', 'SocioController@create');

//Informacion de los socios minimizada
Route::get('socio/min', function(){
	return Socio::select('dni','nombre','apellido')->distinct()->get();
});
Route::get('socio/{dni}/cuotas_impagas', 'SocioController@cuotasImpagas');

Route::get('socio/morosos','SocioController@sociosMorosos');
Route::get('socio/morososPDF','SocioController@sociosMorososPDF');
Route::resource('socio','SocioController');

Route::get('concurrente/obra_social', function(){
	return Concurrente::select('obra_social')->distinct()->get()->pluck('obra_social');
});
Route::resource('concurrente','ConcurrenteController');

Route::get('cuota/valor','CuotaController@obtenerValor');
Route::post('cuota/valor','CuotaController@actualizarValor');
Route::post('cuota/all','CuotaController@storeAll');
Route::resource('cuota','CuotaController');

Route::get('/configuracion', function () {
    return view('configuracion');
});

Route::get('/configuracion/dbdump', 'ConfiguracionController@dbdump');

