var controlador_socios = new Vue({
   el: '#controlador_socios',
   data: {
      creando_socio: false,
      socio_nuevo: {
         dni: '',
         nombre: '',
         apellido: '',
         domicilio: '',
         mail: '',
         telefono: '',
      },
      socios: [],
      busqueda: '',
      mostrar_todos: true,
   },
   methods: {
      recuperarSocios: function(){
         this.$http.get('socio').then(function(respuesta){
            this.socios = respuesta.body;
         }, function(){
            $.notify("No se han podido recuperar los socios", "warn");
         });
      },
   
      createSocio: function(){
         this.$http.post('socio', this.socio_nuevo).then(function(){
            //Guardar nombre y apellido para el mensaje 
            var nombre_apellido = this.socio_nuevo.nombre +" "+this.socio_nuevo.apellido;
            //Blanquear campos
            this.socio_nuevo.dni = '';
            this.socio_nuevo.nombre = '';
            this.socio_nuevo.apellido = '';
            this.socio_nuevo.domicilio = '';
            this.socio_nuevo.mail = '';
            this.socio_nuevo.telefono = '';
            this.recuperarSocios();
            this.creando_socio = false;
            $.notify("El socio "+ nombre_apellido + " se ha registrado correctamente", "success");
         }, function(respuesta){
            $.notify("No se ha podido crear el nuevo socio", "error");
         });
      },

      eliminarSocio: function( nombre_apellido, dni_socio){
         bootbox.confirm({
            message: "¿Está seguro que desea eliminar al socio "+nombre_apellido+"?",
            buttons: {
              confirm: {
                  label: 'Eliminar del sistema',
                  className: 'btn-danger'
              },
              cancel: {
                  label: 'Cancelar',
                  className: 'btn-secondary'
              }
            },
            callback: function(result){
               //Result es true o false
               if(result){
                  controlador_socios.$http.delete('/socio/'+dni_socio).then(function(){
                     controlador_socios.recuperarSocios();
                     $.notify("El socio ha sido eliminado correctamente", "success");
                  }, function(){
                     $.notify("No se ha podido eliminar el socio", "error");
                  });
               }
            }});
      },

      vistaDetallada: function(dni_socio){
         window.location.href = '/vistaSocios/'+dni_socio;
      }

   },

   computed: {
    socios_lista: function(){
      return this.socios.filter((socio) => {
         return (
            socio.dni.toString().match(this.busqueda) 
         || socio.nombre.toLowerCase().match(this.busqueda.toLowerCase())
         || socio.apellido.toLowerCase().match(this.busqueda.toLowerCase())
         || (socio.nombre + " " + socio.apellido).toLowerCase().match(this.busqueda.toLowerCase())
            )
         && (this.mostrar_todos ? true : socio.moroso);
      })
    }
  },

   created: function(){
      this.recuperarSocios();
   }
});