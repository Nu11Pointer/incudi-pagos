
var controlador_concurrente_detalles = new Vue({
   el: '#controlador_concurrente_detalles',
   data: {
      dni : dni_concurrente,
      editando_concurrente: false,
      concurrente: '',
      nueva_cuota: {
         mes : '',
         anio : '',
         fecha : '',
         importe: '',
      },
   },
   methods: {
      recuperarConcurrente: function(){
         this.$http.get('/concurrente/'+this.dni).then(function(respuesta){
            this.concurrente = respuesta.body;
            this.inicializar_form_concurrente(this.concurrente.a_cargo);
         }, function(){
            $.notify("No se ha podido recuperar el concurrente con DNI "+dni_concurrente, "warn");
         });
      },

      crearConcurrente: function(){
         // var fecha_parseada = Date.parse(this.concurrente_nuevo.fecha_ingreso).toString("yyyy-MM-dd");
         // this.concurrente_nuevo.fecha_ingreso = fecha_parseada;
         this.$http.post('/concurrente', this.concurrente_nuevo).then(function(){
            //Blanquear campos
            this.concurrente_nuevo.dni = '',
            this.concurrente_nuevo.a_cargo = '',
            this.concurrente_nuevo.nombre = '',
            this.concurrente_nuevo.apellido = '',
            this.concurrente_nuevo.fecha_ingreso = '',
            this.concurrente_nuevo.obra_social = '',
            this.concurrente_nuevo.servicio = '',

            this.recuperarConcurrentes();
            this.creando_concurrente = false;
         }, function(respuesta){
            alert('No se ha podido crear el nuevo concurrente.');
         });
      },

      eliminarConcurrente: function(dni_concurrente){
         this.$http.delete('/concurrente/'+dni_concurrente).then(function(){
            window.location.href = '/vistaConcurrentes';
         }, function(){
            $.notify("No se ha podido eliminar el concurrente", "error");
         });
      },

      inicializar_form_concurrente: function(a_cargo){

         //Inicializar campo DNI socio a cargo
         this.$http.get('/socio/min').then(function(respuesta){
               respuesta.body.forEach(function(socio) {
                  if(socio.dni != a_cargo){
                     $('#select_dni_socios').append($('<option>', {
                        "data-tokens": socio.nombre +" " + socio.apellido,
                        text: socio.dni
                     }));
                  }
                  else{
                     $('#select_dni_socios').append($('<option>', {
                        selected: '', //Horrible pero asi anda
                        "data-tokens": socio.nombre +" " + socio.apellido,
                        text: socio.dni
                     }));
                  }
               });

               $('#select_dni_socios').selectpicker('refresh');

               //Activando tooltips
               //Lo pongo acá porque asi carga de forma más "perezosa" hasta que aparezcan los objetos en el DOM
               $('[data-toggle="tooltip"]').tooltip();

               //Recargar select servicio
               $('#select_servicio').selectpicker('refresh');

         }, function(){
            alert('No se han podido recuperar los DNI de los socios para listarlos.');
         });

         //Autocomplete obra social
         this.$http.get('/concurrente/obra_social').then(function(respuesta){
               autocompletantes = [];
               var len = respuesta.body.length;
               for (var i = 0; i < len; i++) {
                   autocompletantes.push({
                       id: i,
                       name: respuesta.body[i]
                   });
               }
               $('#input_obra_social').typeahead({
                 source: autocompletantes
               });
            });  

      },

      editarConcurrente: function(){
         //el atributo dni viene desde el servidor
         this.$http.put('/concurrente/'+this.dni, this.concurrente).then(function(){
            this.dni = this.concurrente.dni
            $.notify("El concurrente se ha editado correctamente", "success");
            this.desactivarEdicion();
         }, function(respuesta){
            $.notify("No se ha podido editar el concurrente", "error");
         });
      },

      activarEdicion: function(){
         this.editando_concurrente = true;
         $('.form-control').removeAttr("readonly");

         $('.form-control').removeAttr("disabled");

         //Activando tooltips
         $('#select_dni_socios').selectpicker('refresh');
         //Recargar select servicio
         $('#select_servicio').selectpicker('refresh');
      },

      desactivarEdicion: function(){
         this.editando_concurrente = false;
         $('.form-control').attr("readonly",true);

         $('.form-control').attr("disabled",true);

         //Activando tooltips
         $('#select_dni_socios').selectpicker('refresh');
         //Recargar select servicio
         $('#select_servicio').selectpicker('refresh');
      },

      cancelarEdicion: function(){
         location.reload();
      },

      crearCuota: function(){
         this.nueva_cuota.id_concurrente = this.dni;
         this.$http.post('/cuota', this.nueva_cuota).then(function(response){
            //Blanquear campos
            this.nueva_cuota.anio = '';
            this.nueva_cuota.fecha = '';
            this.nueva_cuota.importe = '';

            rta = response;

            this.recuperarConcurrente();
            $.notify("Nueva cuota agregada", "success");
         }, function(respuesta){
            $.notify("No se ha podido crear la nueva cuota", "error");
         });
      },

      eliminarCuota: function(id_cuota){
         bootbox_borrar(
            '¿Está seguro que desea borrar la cuota?',
            function(){
               controlador_concurrente_detalles.$http.delete('/cuota/'+id_cuota).then(function(){
                  $.notify("La cuota se ha eliminado", "success");
                  controlador_concurrente_detalles.recuperarConcurrente();
               }, function(){
                  $.notify("No se ha podido eliminar la cuota", "error");
               });
            }
         );
      },
   
   },
   created: function(){
      this.recuperarConcurrente();
      //this.inicializar_form_concurrente(); Lo hace el callback del anterior
   },
});


