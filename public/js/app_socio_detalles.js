var controlador_socio_particular = new Vue({
   el: '#controlador_socio_particular',
   data: {
      dni: dni,
      editando_socio: false,
      socio: '',
      busqueda: '',
   },
   methods: {
      recuperarSocio: function(){
         this.$http.get('/socio/'+dni).then(function(respuesta){
            this.socio = respuesta.body;
         }, function(){
            alert('No se han podido recuperar los socios.');
         });
      },
      eliminarSocio: function(nombre_apellido){
         bootbox_borrar(
            "¿Está seguro que desea eliminar al socio?",
            function(){               
               controlador_socio_particular.$http.delete('/socio/'+dni).then(function(response){
                  window.location.href = '/'
               }, function(){
                  $.notify("No se ha podido eliminar el socio", "error");
               });
            });
      },

      editarSocio: function(){
         this.editando_socio = false;
         $('.form-control').attr("readonly", "readonly");
         
         //el atributo dni viene desde el servidor
         this.$http.put('/socio/'+this.dni, this.socio).then(function(){
            this.dni = this.socio.dni
            $.notify("El socio se ha editado correctamente", "success");
         }, function(respuesta){
            $.notify("No se ha podido editar el socio", "error");
         });
      },

      activarEdicion: function(){
         this.editando_socio = true;
         $('.form-control').removeAttr("readonly");
      },

      cancelarEdicion: function(){
         location.reload();
      },

      eliminarPago: function(n_recibo){
         bootbox_borrar(
         "¿Está seguro que desea eliminar el pago "+n_recibo+"?\nLas cuotas correspondientes a este pago volverán a registrarse como impagas.",
         function(result){
            controlador_socio_particular.$http.delete('/pago/'+n_recibo).then(function(){
               $.notify("El pago se ha eliminado correctamente. Las cuotas se registrarán como impagas.", "success");
               controlador_socio_particular.recuperarSocio();
            }, function(){
               $.notify("No se ha podido eliminar el pago", "error");
            });
         });
      },
   },

   created: function(){
      this.recuperarSocio();
   }
});