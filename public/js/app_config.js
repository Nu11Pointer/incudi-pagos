var controlador_configuracion = new Vue({
   el: '#controlador_configuracion',
   data: {
      lista_valor_cuota : [],
      prueba: 0,
      nueva_cuota: {
         mes:'',
         anio:'',
      },
   },
   methods: {
      recuperarValorCuota: function(){
         this.$http.get('/cuota/valor').then(function(respuesta){
            this.lista_valor_cuota = respuesta.body;
         }, function(){
            $.notify('No se han podido recuperar los valores de las cuotas.',"error");
         });
      },

      actualizarValorCuota: function(servicio){
         //Hay que buscar el nuevo valor
         var tipo_cuota_cambio = this.lista_valor_cuota.find( valor_cuota => valor_cuota.servicio === servicio);

         this.$http.post('/cuota/valor', tipo_cuota_cambio).then(function(result){
            this.recuperarValorCuota();
            $.notify('Valores actualizados correctamente.',"success");
         }, function(respuesta){
            $.notify('No se han podido actualizar el valor de las cuotas.',"error");
         });
      },

      agregarCuotasAll: function(){
         this.$http.post('/cuota/all', this.nueva_cuota).then(function(){
            $.notify("Cuota de "+this.nueva_cuota.mes+" de "+this.nueva_cuota.anio+" agregada a todos los concurrentes", "success");
            //Blanquear campos
            this.nueva_cuota.mes = '';
            this.nueva_cuota.anio = '';
         }, function(){
            $.notify("No se han podido agregar las cuotas", "error");
         });
      },

   },

   created: function(){
      this.recuperarValorCuota();
   }
});