var controlador_concurrentes = new Vue({
   el: '#controlador_concurrentes',
   data: {
      creando_concurrente: false,
      concurrente_nuevo: {
         dni: '',
         a_cargo: '',
         nombre: '',
         apellido: '',
         fecha_ingreso: '',
         obra_social: '',
         servicio: '',
      },
      concurrentes: [],
      busqueda: '',
      mostrar_todos: true,
   },
   methods: {
      recuperarConcurrentes: function(){
         this.$http.get('/concurrente').then(function(respuesta){
            this.concurrentes = respuesta.body;
         }, function(){
            alert('No se han podido recuperar los concurrentes.');
         });
      },
   
      crearConcurrente: function(){
         this.$http.post('/concurrente', this.concurrente_nuevo).then(function(){
            $.notify('Se ha creado el nuevo concurrente '+this.concurrente_nuevo.nombre+' '+this.concurrente_nuevo.apellido+'.','success');
            
            //Blanquear campos
            this.concurrente_nuevo.dni = '',
            this.concurrente_nuevo.a_cargo = '',
            this.concurrente_nuevo.nombre = '',
            this.concurrente_nuevo.apellido = '',
            this.concurrente_nuevo.fecha_ingreso = '',
            this.concurrente_nuevo.obra_social = '',
            this.concurrente_nuevo.servicio = '',

            this.recuperarConcurrentes();
            this.creando_concurrente = false;
         }, function(respuesta){
            $.notify('No se ha podido crear el nuevo concurrente.','warning');
         });
      },

      eliminarConcurrente: function(dni_concurrente){
         bootbox_borrar('Está seguro que desea eliminar al concurrente?',
            function(){
            controlador_concurrentes.$http.delete('/concurrente/'+dni_concurrente).then(function(){
               controlador_concurrentes.recuperarConcurrentes();
            }, function(){
               $.notify('No se ha podido eliminar el concurrente.','error');
            });
         });
      },

      inicializar_form_concurrente: function(){
         //Inicializar campo DNI socio a cargo
         this.$http.get('/socio/min').then(function(respuesta){
               respuesta.body.forEach(function(socio) {
                  $('#select_dni_socios').append($('<option>', {
                     "data-tokens": socio.nombre +" " + socio.apellido,
                     text: socio.dni
                  }));
               });

               $('#select_dni_socios').selectpicker('refresh');

               //Activando tooltips
               //Lo pongo acá porque asi carga de forma más "perezosa" hasta que aparezcan los objetos en el DOM
               $('[data-toggle="tooltip"]').tooltip();

               //Recargar select servicio
               $('#select_servicio').selectpicker('refresh');

               //Colocar fecha del dia de hoy
               var d = new Date()
               $('#datepicker_fecha_ingreso').val(d.toISOString().split('T')[0]);

         }, function(){
            alert('No se han podido recuperar los DNI de los socios para listarlos.');
         });

         //Autocomplete obra social
         this.$http.get('/concurrente/obra_social').then(function(respuesta){
               autocompletantes = [];
               var len = respuesta.body.length;
               for (var i = 0; i < len; i++) {
                   autocompletantes.push({
                       id: i,
                       name: respuesta.body[i]
                   });
               }
               $('#input_obra_social').typeahead({
                 source: autocompletantes
               });
            });  

      },
   },

   computed: {
    concurrentes_lista: function(){
      return this.concurrentes.filter((concurrente) => {
         return (
            concurrente.dni.toString().match(this.busqueda) 
         || concurrente.nombre.toLowerCase().match(this.busqueda.toLowerCase())
         || concurrente.apellido.toLowerCase().match(this.busqueda.toLowerCase())
         || (concurrente.nombre + " " + concurrente.apellido).toLowerCase().match(this.busqueda.toLowerCase())
            )
         && (this.mostrar_todos ? true : concurrente.moroso);
      })
    }
  },

   created: function(){
      this.recuperarConcurrentes();
   }
});