var controlador_pago = new Vue({
   el: '#controlador_pago',
   data: {
      pago: {
         id_socio: dni_socio,
         pagador: '',
         importe: '',
         fecha_pago: '',
         concepto: '',
         cuotas_pagar: [],
      },
      socio: '',
      cuotas_impagas: [],
      pago_realizado: false,
      n_recibo : '',
   },
   methods: {
      recuperarCuotasImpagas: function(){
         this.$http.get('/socio/'+dni_socio+'/cuotas_impagas').then(function(respuesta){
            this.cuotas_impagas = respuesta.body;
         }, function(){
            $.notify("No se han podido recuperar las cuotas impagas", "warn");
         });
      },
   
      crearPago: function(){
         this.$http.post('/pago/'+dni_socio, this.pago).then(function(respuesta){
            $.notify("El pago se ha realizado correctamente", "success");
            this.n_recibo = respuesta.body.n_recibo;
            this.pago_realizado = true;
         }, function(respuesta){
            rta=respuesta;
            $.notify("No se ha podido efectuar el pago", "error");
         });
      },

      recuperarSocio: function(){
         this.$http.get('/socio/'+dni_socio).then(function(respuesta){
            this.socio = respuesta.body;
            this.pago.pagador = this.socio.nombre + " " + this.socio.apellido;
         }, function(){
            alert('No se han podido recuperar los socios.');
         });
      },

      registrar_cuotas_pagar: function(id_cuota, event){
         if (event.target.checked){
            this.pago.cuotas_pagar.push(id_cuota);
            $.notify("Se registrará el pago de la cuota con id:"+id_cuota, "info");
         }
         else{
            var index = this.pago.cuotas_pagar.indexOf(id_cuota);
            if (index > -1) {
              this.pago.cuotas_pagar.splice(index, id_cuota);
            }
            $.notify("Retirando del pago la cuota con id:"+id_cuota, "info");
         }
         this.actualizar_pago();
      },

      actualizar_pago(){
         //Importe
         var importe_preliminar = 0;
         //String con fechas de las cuotas a pagar para poner en el concepto
         var fechas_cuotas_pagar = '';
         //Recorro el arreglo para buscar los atributos deseados de las cuotas a pagar
         this.pago.cuotas_pagar.forEach(function(id_cuota_a_pagar){
            controlador_pago.cuotas_impagas.filter(cuota => {
               if(cuota.id_cuota == id_cuota_a_pagar){
                  importe_preliminar = importe_preliminar + cuota.importe;
                  fechas_cuotas_pagar = fechas_cuotas_pagar + cuota.fecha_human + ', ';
               }
            });
         });
         this.pago.importe = importe_preliminar;

         //Concepto
         //Elimino el ', ' del final del string usados para separar
         fechas_cuotas_pagar = fechas_cuotas_pagar.slice(0, -2);
         //Agrego punto final
         fechas_cuotas_pagar = fechas_cuotas_pagar + '.'
         //Finalmente completo el campo "concepto"
         this.pago.concepto = "pago de cuota "+fechas_cuotas_pagar;
      }

   },

   created: function(){
      this.recuperarCuotasImpagas();
      this.recuperarSocio();

      //Fecha de pago (hoy)
      var d = new Date();
      this.pago.fecha_pago = d.toISOString().split('T')[0];

;   }
});