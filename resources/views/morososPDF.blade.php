<!DOCTYPE html>

<html lang="es">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <!-- <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}" media="all" /> -->
    <title>Morosos</title>
<style>
body {
    font-family: Arial, Helvetica, sans-serif;
    /*font-size: 13px;*/
}

table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
</style>
</head>


<body>

<h1>Socios morosos</h1>
<h7>INCUDI <i>pagos</i></h7>

<table style="width:100%">
  <tr>
    <th>DNI</th>
    <th>Nombre</th> 
    <th>Apellido</th>
    <th>Domicilio</th>
    <th>Mail</th>
    <th>Teléfono</th>
  </tr>
  @foreach($morosos as $moroso)
    <tr>
      <td>{{$moroso->dni}}</td>
      <td>{{$moroso->nombre}}</td> 
      <td>{{$moroso->apellido}}</td>
      <td width="25%">{{$moroso->domicilio}}</td>
      <td>{{$moroso->mail}}</td>
      <td>{{$moroso->telefono}}</td>
    </tr>
  @endforeach
</table>
<h3>{{$fecha_hora}}</h3>

</body>

</html>