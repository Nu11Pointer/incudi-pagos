@extends('layouts.layout_incudi_pagos')

@section('content')

<script type="text/javascript">
  dni_socio = {{ $dni_socio }};
</script>

<link rel="stylesheet" href="/css/pago.css">

<body>
<div action="javascript:void(0);" id="controlador_pago">

<div class="container cont_form" v-if="!pago_realizado">
    <h2>Generando pago</h2>

    <div class="row">

    <div class="input-group mb-3 col-sm">
      <div class="input-group-prepend">
        <span class="input-group-text">Recibí de</span>
      </div>
      <input type="text" class="form-control" placeholder="Nombre de quien paga" v-model="pago.pagador">
    </div>

    </div> <!-- End row -->

    <div class="row">

    <div class="input-group mb-3 col-sm">
      <div class="input-group-prepend">
        <span class="input-group-text">La cantidad de</span>
      </div>
      <input type="number" class="form-control" placeholder="Importe" v-model="pago.importe">
    </div>

    </div> <!-- End row -->

    <div class="row">
    <div class="input-group mb-3 col-md-4">
      <div class="input-group-prepend">
        <span class="input-group-text">Fecha de pago</span>
      </div>
      <input type="date" class="form-control" placeholder="Seleccione una fecha" id="datepicker_fecha_pago" v-model="pago.fecha_pago">
    </div>
    </div> <!-- End row -->

    <div class="row">
    <div class="input-group mb-3 col-md">
      <div class="input-group-prepend">
        <span class="input-group-text">En concepto de</span>
      </div>
      <input type="fecha" class="form-control" placeholder="Concepto" v-model="pago.concepto">
    </div>
    </div> <!-- End row -->

    <table class="table table-hover">
	  <thead>
	    <tr>
	      <th scope="col">Identificador</th>
	      <th scope="col">Concurrente</th>
	      <th scope="col">Fecha</th>
	      <th scope="col">Importe</th>
	      <th scope="col">Pagar</th>
	    </tr>
	  </thead>

	  <tbody>
	    <tr v-for="cuota in cuotas_impagas"  :key="cuota" :class="{ 'moroso' : cuota.vencida }">
	      <td>@{{ cuota.id_cuota }}</td>
	      <td>@{{ cuota.nombre_concurrente }}</td>
	      <td>@{{ cuota.fecha_human }}</td>
	      <td>@{{ cuota.importe }}</td>
	      <td><input class="form-check-input" type="checkbox" value="" v-on:click="registrar_cuotas_pagar(cuota.id_cuota,$event)"></td>
	    </tr>
	  </tbody>
	</table>

    <button class="btn btn-success btn-block" v-on:click="crearPago()"> Pagar </button>

</div> <!-- Main container -->

<div v-else>
    <div class="container text-center">
      <br><br><br><br>
      <h1 class="text-center">El pago se ha efectuado correctamente</h1>
      <br><br>
      <button class="btn btn-light btn-lg" v-on:click="window.open('/pago/recibo/'+n_recibo)">Obtener el recibo</button>
  </div>
</div>

</div> <!-- Fin app VUE-->

</body>

<script type="text/javascript" src="/js/vue.min.js"></script>
<script type="text/javascript" src="/js/vue-resource.min.js"></script>
<script type="text/javascript" src="/js/app_pago.js"></script>

@endsection