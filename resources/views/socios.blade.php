@extends('layouts.layout_incudi_pagos')

@section('content')

<body>
  <div class="container" action="javascript:void(0);" id="controlador_socios">
  
  <!-- Container Form-->
  <div class="container-fluid cont_form">
  <div v-if="creando_socio">
    <h2>Creando nuevo socio</h2>
    <div class="row">
    <div class="input-group mb-3 col-sm-3">
      <div class="input-group-prepend">
        <span class="input-group-text">DNI</span>
      </div>
      <input type="number" class="form-control" placeholder="DNI" aria-label="DNI del nuevo socio" aria-describedby="basic-addon1" v-model="socio_nuevo.dni">
    </div>
    </div>

    <div class="row">
    <div class="input-group mb-3 col-sm">
      <div class="input-group-prepend">
        <span class="input-group-text" v-model="socio_nuevo.nombre">Nombre</span>
      </div>
      <input type="text" class="form-control" placeholder="Nombre del nuevo socio" v-model="socio_nuevo.nombre">
    </div>
    

    <div class="input-group mb-3 col-sm">
      <div class="input-group-prepend">
        <span class="input-group-text">Apellido</span>
      </div>
      <input type="text" class="form-control" placeholder="Apellido del nuevo socio" v-model="socio_nuevo.apellido">
    </div>

    </div>

    <div class="row">

    <div class="input-group mb-3 col-sm">
      <div class="input-group-prepend">
        <span class="input-group-text">Domicilio</span>
      </div>
      <input type="text" class="form-control" placeholder="Dirección del nuevo socio" v-model="socio_nuevo.domicilio">
    </div>

    </div>

    <div class="row">
    <div class="input-group mb-3 col-sm">
      <div class="input-group-prepend">
        <span class="input-group-text">Mail</span>
      </div>
      <input type="email" class="form-control" placeholder="Email del nuevo socio" v-model="socio_nuevo.mail">
    </div>
    

    <div class="input-group mb-3 col-sm">
      <div class="input-group-prepend">
        <span class="input-group-text">Teléfono</span>
      </div>
      <input type="number" class="form-control" placeholder="Teléfono del nuevo socio" v-model="socio_nuevo.telefono">
    </div>
    </div>

    <button class="btn btn-success" data-dismiss="alert" v-on:click="createSocio()"> Guardar </button>
    <button class="btn btn-secondary" v-on:click="creando_socio = false"> Canelar </button>

  </div>

  <div v-else class="text-center">
    <button type="submit" class="btn btn-primary" v-on:click="creando_socio = true">Agregar un nuevo socio</button>
  </div>
  </div>

<div class="row">
  <div class="input-group mb-3 col-sm-9">
    <div class="input-group-prepend">
      <span class="input-group-text">Filtrar</span>
    </div>
    <input type="text" class="form-control" v-model="busqueda">
  </div>

  <div class="input-group mb-3 col-sm">
    <form class="was-validated">
    <div class="custom-control custom-checkbox mb-3">
      <input type="checkbox" class="custom-control-input" id="mostrar_todos" v-model="mostrar_todos" required checked>
      <label class="custom-control-label active" for="mostrar_todos">Mostrar todos los socios</label>
      <div class="invalid-feedback">Sólo se muestran socios morosos</div>
    </div>
    </form>
  </div>
</div>

<table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">DNI</th>
      <th scope="col">Nombre</th>
      <th scope="col">Apellido</th>
      <th scope="col">Domicilio</th>
      <th scope="col">Mail</th>
      <th scope="col">Teléfono</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
       <!-- Fila para modificar una tarea. -->
    <tr v-for="soc in socios_lista" :key="soc" :class="{ 'moroso' : soc.moroso }">
      <td>@{{ soc.dni }}</td>
      <td>@{{ soc.nombre }}</td>
      <td>@{{ soc.apellido }}</td>
      <td>@{{ soc.domicilio }}</td>
      <td>@{{ soc.mail }}</td>
      <td>@{{ soc.telefono }}</td>
      <td><button class="btn btn-info" v-on:click="vistaDetallada(soc.dni)"> Detalles </button>
      <button class="btn btn-danger"
       v-on:click="eliminarSocio(soc.nombre+' '+soc.apellido,soc.dni)" data-toggle="confirmation"> Eliminar </button>
    </td>
    </tr>
  </tbody>
</table>


</div> <!-- Fin container -->
</body>

<script type="text/javascript" src="/js/vue.min.js"></script>
<script type="text/javascript" src="/js/vue-resource.min.js"></script>
<script type="text/javascript" src="/js/app_socios.js"></script>

@endsection