<!DOCTYPE html>
<html lang="es">
<html>
<head>
  <!-- JQuery -->
<!--   <script src="/js/jquery-3.3.1.slim.min.js" 
  integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" 
  crossorigin="anonymous"></script> -->
  <script src="/js/jquery-3.3.1.js"></script>

  <!-- Popper -->
  <script src="/js/popper.min.js"></script>

  <!-- Bootstrap -->
  <link rel="stylesheet" href="/css/bootstrap.min.css">

  <!-- Bootstrap JS -->
  <script src="/js/bootstrap.min.js"></script>

  <!-- jQuery Notify -->
  <script src="/js/notify.min.js"></script>

  <!-- Estilo -->
  <link rel="stylesheet" href="/css/style.css">

  <!-- Bootbox (dialogos eliminar)-->
  <script type="text/javascript" src="/js/bootbox.min.js"></script>

  <!-- Favicon -->
  <link rel="shortcut icon" href="/img/favicon.png" />
  
  <!-- Titulo -->
  <title>INCUDI</title>

  <!-- Navbar -->
 <nav class="navbar navbar-expand-lg">
  
  <div class="container">

    <a class="navbar-brand" href="/">INCUDI <i>Pagos</i></a> 
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="/">Socios</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/vistaConcurrentes">Concurrentes</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/socio/morososPDF" target="_blank">Listado morosos</a>
        </li>
      </ul>
      <span class="navbar-text">
        <a class="nav-link" href="/configuracion">Configuración</a>
      </span>
    </div>

  </div> <!-- End cointainer -->
</nav>
</head>

  @yield('content')

</html>