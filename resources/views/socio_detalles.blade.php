@extends('layouts.layout_incudi_pagos')

@section('content')

<script type="text/javascript">
  dni = {{ $dni }};
</script>

<body>
<div class="container" id="controlador_socio_particular">

<div class="container-fluid cont_form">

  <div class="cont_form text-center">
    <button type="submit" class="btn btn-success btn-lg" v-on:click="location.href='/pago/'+socio.dni">Pagar</button>
  </div>

  <div class="row">
  <div class="input-group mb-3 col-sm-3">
    <div class="input-group-prepend">
      <span class="input-group-text">DNI</span>
    </div>
    <input type="number" class="form-control" placeholder="DNI" aria-label="DNI del nuevo socio" aria-describedby="basic-addon1" v-model="socio.dni" readonly>
  </div>
  </div>

  <div class="row">
  <div class="input-group mb-3 col-sm">
    <div class="input-group-prepend">
      <span class="input-group-text">Nombre</span>
    </div>
    <input type="text" class="form-control" placeholder="Nombre del nuevo socio" v-model="socio.nombre" readonly>
  </div>
  

  <div class="input-group mb-3 col-sm">
    <div class="input-group-prepend">
      <span class="input-group-text">Apellido</span>
    </div>
    <input type="text" class="form-control" placeholder="Apellido del nuevo socio" v-model="socio.apellido" readonly>
  </div>

  </div>

  <div class="row">

  <div class="input-group mb-3 col-sm">
    <div class="input-group-prepend">
      <span class="input-group-text">Domicilio</span>
    </div>
    <input type="text" class="form-control" placeholder="Dirección del nuevo socio" v-model="socio.domicilio" readonly>
  </div>

  </div>

  <div class="row">
  <div class="input-group mb-3 col-sm">
    <div class="input-group-prepend">
      <span class="input-group-text">Mail</span>
    </div>
    <input type="email" class="form-control" placeholder="Email del nuevo socio" v-model="socio.mail" readonly>
  </div>
  

  <div class="input-group mb-3 col-sm">
    <div class="input-group-prepend">
      <span class="input-group-text">Teléfono</span>
    </div>
    <input type="number" class="form-control" placeholder="Teléfono del nuevo socio" v-model="socio.telefono" readonly>
  </div>
  </div>


  <button class="btn btn-primary" data-dismiss="alert" v-on:click="activarEdicion()" v-if="!editando_socio"> Editar </button>
  <button class="btn btn-success" data-dismiss="alert" v-on:click="editarSocio(dni)" v-if="editando_socio"> Guardar </button>
  <button class="btn btn-secondary" v-on:click="cancelarEdicion()" v-if="editando_socio"> Canelar </button>

  <button class="btn btn-danger float-right" v-on:click="eliminarSocio()"> Eliminar </button>

</div> <!-- Fin container form -->

<hr/>
<div v-if="socio.concurrentes && socio.concurrentes.length != 0">
  <h2>Pagos realizados</h2>
  <table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">Recibo nº</th>
        <th scope="col">De</th>
        <th scope="col">Importe</th>
        <th scope="col">Fecha</th>
        <th scope="col">Concepto</th>
        <th scope="col">Opciones</th>
      </tr>
    </thead>
    <tbody>
         <!-- Fila para modificar una tarea. -->
      <tr v-for="pago in socio.pagos">
        <td>@{{ pago.n_recibo }}</td>
        <td>@{{ pago.pagador }}</td>
        <td>@{{ pago.importe }}</td>
        <td>@{{ pago.fecha_pago }}</td>
        <td>@{{ pago.concepto }}</td>
        <td><button class="btn btn-light" v-on:click="window.open('/pago/recibo/'+pago.n_recibo)"> Recibo </button>
            <button class="btn btn-danger" v-on:click="eliminarPago(pago.n_recibo)"> Eliminar </button></td>
      </tr>
    </tbody>
  </table>
</div>


<hr/>
<div v-if="socio.concurrentes && socio.concurrentes.length != 0">
  <h2>Concurrente a cargo</h2>
  <table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">DNI Concurrente</th>
        <th scope="col">Nombre</th>
        <th scope="col">Apellido</th>
        <th scope="col">Obra Social</th>
        <th scope="col">Servicio</th>
        <th scope="col"></th>
      </tr>
    </thead>
    <tbody>
         <!-- Fila para modificar una tarea. -->
      <tr v-for="conc in socio.concurrentes">
        <td>@{{ conc.dni }}</td>
        <td>@{{ conc.nombre }}</td>
        <td>@{{ conc.apellido }}</td>
        <td>@{{ conc.obra_social }}</td>
        <td>@{{ conc.servicio }}</td>
        <td><button class="btn btn-info" v-on:click="location.href='/vistaConcurrentes/'+conc.dni"> Detalles </button></td>
      </tr>
    </tbody>
  </table>
</div>

<h2 v-else>El socio no posee concurrentes a su cargo</h2>


</div> <!-- Fin container -->
</body>

<script type="text/javascript" src="/js/vue.min.js"></script>
<script type="text/javascript" src="/js/vue-resource.min.js"></script>
<script type="text/javascript" src="/js/app_socio_detalles.js"></script>

@endsection