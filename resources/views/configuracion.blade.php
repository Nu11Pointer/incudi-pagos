@extends('layouts.layout_incudi_pagos')

@section('content')

<link rel="stylesheet" href="/css/configuracion.css">

<body>
<div class="container" action="javascript:void(0);" id="controlador_configuracion">
  <br>
  <br>
<div class="jumbotron">
  <h1 class="display-4">Menú de configuración</h1>
  <p class="lead">Configuración del sistema INCUDI <i>pagos</i></p>
  
  <hr class="my-4">
  <p>Con esta opción se generará un archivo de copia de seguridad de la base de datos, que servirá para restaurarla en caso de sufrir un inconveniente con el sistema o el equipo que lo soporta.</p>
  <p class="lead">
    <a class="btn btn-primary btn-lg btn-dark" href="/configuracion/dbdump" role="button">Descargar copia de seguridad</a>
  </p>

  <hr class="my-4">
  <p>Configurar el valor de los tipos de cuotas</p>

  <div class="input-group mb-3 " v-for="valor_cuota in lista_valor_cuota">
    <input type="number" class="form-control" placeholder="Nuevo importe" v-model="valor_cuota.nuevo_valor">
    <div class="input-group-append">
      <button class="btn btn-success" type="button" v-on:click="actualizarValorCuota(valor_cuota.servicio)">Actualizar valor "@{{ valor_cuota.servicio }}"</button>
      <span class="input-group-text">Importe actual: $@{{ valor_cuota.valor }}</span>
      <!-- <span class="input-group-text">Nuevo valor: $@{{ valor_cuota.nuevo_valor }}</span> -->
    </div>
  </div>

    <hr class="my-4">
      <p>Agregar cuotas a todos los socios.</p>

  <form>
    <div class="row">

      <div class="input-group mb-3 col-md-3">
        <div class="input-group-prepend">
          <span class="input-group-text">Mes</span>
        </div>
        <select class="selectpicker form-control" v-model="nueva_cuota.mes">
          <option>enero</option>
          <option>febrero</option>
          <option>marzo</option>
          <option>abril</option>
          <option>mayo</option>
          <option>junio</option>
          <option>julio</option>
          <option>agosto</option>
          <option>septiembre</option>
          <option>octubre</option>
          <option>noviembre</option>
          <option>diciembre</option>
        </select>
      </div>

      <div class="input-group mb-3 col-md-2">
      <div class="input-group-prepend">
        <span class="input-group-text">Año</span>
      </div>
        <input type="number" class="form-control" placeholder="Año" v-model="nueva_cuota.anio">
      </div>

    </div>
  </form>

  <p class="lead">
    <a class="btn btn-primary btn-lg btn-warning" role="button" v-on:click="agregarCuotasAll()">Agregar cuotas</a>
  </p>
  
</div> <!-- END JUMBOTRON -->

<br>
<br>
<br>

</div>

<nav class="navbar fixed-bottom navbar-dark bg-dark">
  <a class="navbar-brand" href="mailto:federico.paganetto@hotmail.com?Subject=Consulta">Sistema creado por Federico Paganetto</a>
</nav>

</body>

<script type="text/javascript" src="/js/vue.min.js"></script>
<script type="text/javascript" src="/js/vue-resource.min.js"></script>
<script type="text/javascript" src="/js/app_config.js"></script>

@endsection