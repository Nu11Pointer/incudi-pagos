<!DOCTYPE html>

<html lang="es">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <!-- <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}" media="all" /> -->
    <title>Recibo</title>
<style>
	body{
	   background-image: url("/img/fondo_recibo.png");
	   background-repeat: no-repeat;
	}
</style>
</head>


<body>



<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:20px 10px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:20px 10px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-kiyw{background-color:#efefef;border-color:inherit;vertical-align:top}
.tg .tg-l711{border-color:inherit}
.tg .tg-uys7{border-color:inherit;text-align:center}
.tg .tg-oqn6{font-weight:bold;font-family:Tahoma, Geneva, sans-serif !important;;background-color:#000000;color:#ffffff;text-align:center;vertical-align:top}
.tg .tg-ytur{font-size:24px;font-family:Arial, Helvetica, sans-serif !important;;background-color:#efefef;border-color:inherit}
.tg .tg-gcw3{border-color:#000000}
</style>
<table class="tg" style="undefined;table-layout: fixed; width: 726px">
<colgroup>
<col style="width: 554px">
<col style="width: 172px">
</colgroup>
  <tr>
    <th class="tg-oqn6" colspan="2" style="padding:5px">Instituto de Custodia y Adaptación para Disminuidos Psicofísicos (INCUDI)</th>
  </tr>
  <tr>
    <td class="tg-ytur">Recibo </td>
    <td class="tg-kiyw">nº {{$pago->n_recibo}}</td>
  </tr>
  <tr>
    <td class="tg-gcw3" colspan="2">Recibí de <span style="font-weight:bold">{{$pago->pagador}}</span> la cantidad de<span style="font-weight:bold"> $ {{$pago->importe}}</span><br>{{$precio_letras}}</td>
  </tr>
  <tr>
    <td class="tg-l711" colspan="2">En concepto de <span style="font-weight:bold">{{$pago->concepto}}</span></td>
  </tr>
  <tr>
    <td class="tg-uys7" colspan="2">Fecha: {{$fecha_letras}}<br><br><br><br><br><br><br>_________________________________<br>(firma)</td>
  </tr>
</table>



</body>

</html>