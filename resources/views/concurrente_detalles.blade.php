@extends('layouts.layout_incudi_pagos')

@section('content')

<script type="text/javascript">
  dni_concurrente = {{ $dni_concurrente }};
</script>

<!-- Boostrap Select CSS -->
<link rel="stylesheet" href="/css/bootstrap-select.min.css">

<!-- Boostrap Select JavaScript -->
<script src="/js/bootstrap-select.min.js"></script>

<!-- Autocomplete -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>

<body>
<div class="container" id="controlador_concurrente_detalles">

<!-- Container Form-->
  <div class="container-fluid cont_form">

    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <h2>Detalles del concurrente</h2>

    <div class="row">

    <div class="input-group mb-3 col-sm-3">
      <div class="input-group-prepend">
        <span class="input-group-text" v-model="concurrente.nombre">DNI</span>
      </div>
      <input type="number" class="form-control" placeholder="DNI" v-model="concurrente.dni" readonly>
    </div>
    
    <div class="input-group mb-3 col-sm-4">
      <div class="input-group-prepend">
        <span class="input-group-text" data-toggle="tooltip" data-placement="top" title="Puede buscar escribiendo el nombre o apellido del socio y aparecerá su DNI registrado." >DNI del socio a cargo</span>
      </div>
        <select class="selectpicker form-control" data-live-search="true" v-model="concurrente.a_cargo" id="select_dni_socios" disabled="disabled">
      </select>
    </div>

    </div> <!-- End row -->



    <div class="row">

    <div class="input-group mb-3 col-sm">
      <div class="input-group-prepend">
        <span class="input-group-text" v-model="concurrente.nombre">Nombre</span>
      </div>
      <input type="text" class="form-control" placeholder="Nombre del nuevo concurrente" v-model="concurrente.nombre" readonly>
    </div>
    

    <div class="input-group mb-3 col-sm">
      <div class="input-group-prepend">
        <span class="input-group-text">Apellido</span>
      </div>
      <input type="text" class="form-control" placeholder="Apellido del nuevo concurrente" v-model="concurrente.apellido" readonly>
    </div>

    </div> <!-- End row -->

    <div class="row">
    <div class="input-group mb-3 col-md-4">
      <div class="input-group-prepend">
        <span class="input-group-text">Fecha de ingreso</span>
      </div>
      <input type="date" class="form-control" placeholder="Seleccione una fecha" id="datepicker_fecha_ingreso" v-model="concurrente.fecha_ingreso" readonly>
    </div>
    

    <div class="input-group mb-3 col-md-4">
      <div class="input-group-prepend">
        <span class="input-group-text" data-toggle="tooltip" data-placement="top" data-html="true" title="Aparecerán opciones al comenzar a escribir.<br> Intente mantener el mismo nombre que los ejemplos (mayúsculas y minúsculas o signos de puntuación).">Obra social</span>
      </div>
      <input type="text" class="form-control" id="input_obra_social" placeholder="Escriba el nombre de la OS" v-model="concurrente.obra_social" readonly>
    </div>

    <div class="input-group mb-3 col-md-4">
      <div class="input-group-prepend">
        <span class="input-group-text">Servicio</span>
      </div>
      <select class="selectpicker form-control" v-model="concurrente.servicio" id="select_servicio" disabled="disabled">
        <option>centro de día 1</option>
        <option>centro de día 2</option>
        <option>residencia</option>
        <option>hogar</option>
      </select>
    </div>

    </div>

  <button class="btn btn-primary" data-dismiss="alert" v-on:click="activarEdicion()" v-if="!editando_concurrente"> Editar </button>
  <button class="btn btn-success" data-dismiss="alert" v-on:click="editarConcurrente(dni)" v-if="editando_concurrente"> Guardar </button>
  <button class="btn btn-secondary" v-on:click="cancelarEdicion()" v-if="editando_concurrente"> Canelar </button>

  <button class="btn btn-danger float-right" v-on:click="eliminarConcurrente(concurrente.dni)"> Eliminar </button>

  <!-- Fin container form-->
  </div>

<hr>

<h2>Cuotas del concurrente</h2>
<table class="table table-hover">
  <thead>
    <tr>
      <!-- <th scope="col">Identificador</th> -->
      <th scope="col">Recibo nº</th>
      <th scope="col">Fecha</th>
      <th scope="col">Importe</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
       <!-- Fila para modificar una tarea. -->
    <tr v-for="cuota in concurrente.cuotas" :key="cuota" :class="{ 'moroso' : cuota.vencida }">
      <!-- <td>@{{ cuota.id_cuota }}</td> -->
      <td>@{{ cuota.n_recibo }}</td>
      <td>@{{ cuota.fecha_human }}</td>
      <td>@{{ cuota.importe }}</td>
      <td>
        <button class="btn btn-success" v-if="cuota.vencida" v-on:click="location.href='/pago/'+concurrente.a_cargo">Pagar</button> 
        <button class="btn btn-danger float-right" v-on:click="eliminarCuota(cuota.id_cuota)">Eliminar </button>
      </td>
    </tr>
  </tbody>
</table>


<hr>
<h2>Crear una cuota</h2>
<form>
  <div class="row">

    <div class="input-group mb-3 col-md-3">
      <div class="input-group-prepend">
        <span class="input-group-text">Mes</span>
      </div>
      <select class="selectpicker form-control" v-model="nueva_cuota.mes">
        <option>enero</option>
        <option>febrero</option>
        <option>marzo</option>
        <option>abril</option>
        <option>mayo</option>
        <option>junio</option>
        <option>julio</option>
        <option>agosto</option>
        <option>septiembre</option>
        <option>octubre</option>
        <option>noviembre</option>
        <option>diciembre</option>
      </select>
    </div>

    <div class="input-group mb-3 col-md-2">
    <div class="input-group-prepend">
      <span class="input-group-text">Año</span>
    </div>
      <input type="number" class="form-control" placeholder="Año" v-model="nueva_cuota.anio">
    </div>

    <div class="col-3">
      <input type="number" class="form-control" placeholder="Importe" v-model="nueva_cuota.importe">
    </div>
    <div class="col-3">
        <button class="btn btn-warning"  data-dismiss="alert" v-on:click="crearCuota()"> Crear nueva cuota </button>
    </div>

  </div>
</form>



<br>
<br>
<br>
<br>


</div> <!-- Fin container -->
</body>

<script type="text/javascript" src="/js/vue.min.js"></script>
<script type="text/javascript" src="/js/vue-resource.min.js"></script>
<script type="text/javascript" src="/js/app_concurrente_detalles.js"></script>

@endsection