@extends('layouts.layout_incudi_pagos')

@section('content')

<!-- Boostrap Select CSS -->
<link rel="stylesheet" href="/css/bootstrap-select.min.css">

<!-- Boostrap Select JavaScript -->
<script src="/js/bootstrap-select.min.js"></script>

<!-- Autocomplete -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>

<body>
  <div class="container" action="javascript:void(0);" id="controlador_concurrentes">

  <!-- Container Form-->
  <div class="container-fluid cont_form">
  <div v-if="creando_concurrente">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <h2>Creando nuevo concurrente</h2>

    <div class="row">

    <div class="input-group mb-3 col-sm-3">
      <div class="input-group-prepend">
        <span class="input-group-text" v-model="concurrente_nuevo.nombre">DNI</span>
      </div>
      <input type="number" class="form-control" placeholder="DNI" v-model="concurrente_nuevo.dni">
    </div>
    
    <div class="input-group mb-3 col-sm-4">
      <div class="input-group-prepend">
        <span class="input-group-text" data-toggle="tooltip" data-placement="top" title="Puede buscar escribiendo el nombre o apellido del socio y aparecerá su DNI registrado.">DNI del socio a cargo</span>
      </div>
        <select class="selectpicker form-control" data-live-search="true" v-model="concurrente_nuevo.a_cargo" id="select_dni_socios" title="Seleccione DNI">
      </select>
    </div>

    </div> <!-- End row -->



    <div class="row">

    <div class="input-group mb-3 col-sm">
      <div class="input-group-prepend">
        <span class="input-group-text" v-model="concurrente_nuevo.nombre">Nombre</span>
      </div>
      <input type="text" class="form-control" placeholder="Nombre del nuevo concurrente" v-model="concurrente_nuevo.nombre">
    </div>
    

    <div class="input-group mb-3 col-sm">
      <div class="input-group-prepend">
        <span class="input-group-text">Apellido</span>
      </div>
      <input type="text" class="form-control" placeholder="Apellido del nuevo concurrente" v-model="concurrente_nuevo.apellido">
    </div>

    </div> <!-- End row -->

    <div class="row">
    <div class="input-group mb-3 col-md-4">
      <div class="input-group-prepend">
        <span class="input-group-text">Fecha de ingreso</span>
      </div>
      <input type="date" class="form-control" placeholder="Seleccione una fecha" id="datepicker_fecha_ingreso" v-model="concurrente_nuevo.fecha_ingreso">
    </div>
    

    <div class="input-group mb-3 col-md-4">
      <div class="input-group-prepend">
        <span class="input-group-text" data-toggle="tooltip" data-placement="top" data-html="true" title="Aparecerán opciones al comenzar a escribir.<br> Intente mantener el mismo nombre que los ejemplos (mayúsculas y minúsculas o signos de puntuación).">Obra social</span>
      </div>
      <input type="text" class="form-control" id="input_obra_social" placeholder="Escriba el nombre de la OS" v-model="concurrente_nuevo.obra_social">
    </div>

    <div class="input-group mb-3 col-md-4">
      <div class="input-group-prepend">
        <span class="input-group-text">Servicio</span>
      </div>
      <select class="selectpicker form-control" v-model="concurrente_nuevo.servicio" id="select_servicio">
        <option>centro de día 1</option>
        <option>centro de día 2</option>
        <option>residencia</option>
        <option>hogar</option>
      </select>
    </div>

    </div>

    <button class="btn btn-success" v-on:click="crearConcurrente()"> Guardar </button>
    <button class="btn btn-secondary" v-on:click="creando_concurrente = false"> Canelar </button>

  </div>

  <div v-else class="text-center">
    <button type="button" class="btn btn-primary" v-on:click="creando_concurrente=true; inicializar_form_concurrente();">Agregar un nuevo concurrente</button>
  </div>

  <!-- Fin container form-->
  </div>

  <div class="row">
  <div class="input-group mb-3 col-sm-9">
    <div class="input-group-prepend">
      <span class="input-group-text">Filtrar</span>
    </div>
    <input type="text" class="form-control" v-model="busqueda">
  </div>

  <div class="input-group mb-3 col-sm">
    <form class="was-validated">
    <div class="custom-control custom-checkbox mb-3">
    <input type="checkbox" class="custom-control-input" id="mostrar_todos" v-model="mostrar_todos" required checked>
      <label class="custom-control-label active" for="mostrar_todos">Mostrar todos los concurrentes</label>
      <div class="invalid-feedback">Solo se muestran concurrentes que adeudan cuotas</div>
    </div>
    </form>
  </div>
</div>

<table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">DNI</th>
      <th scope="col">DNI a cargo</th>
      <th scope="col">Nombre</th>
      <th scope="col">Apellido</th>
      <th scope="col">Opciones</th>
    </tr>
  </thead>
  <tbody>
       <!-- Fila para modificar una tarea. -->
    <tr v-for="conc in concurrentes_lista" :key="conc" :class="{ 'moroso' : conc.moroso }">
      <td>@{{ conc.dni }}</td>
      <td>@{{ conc.a_cargo }}</td>
      <td>@{{ conc.nombre }}</td>
      <td>@{{ conc.apellido }}</td>
      <td>
        <button class="btn btn-info" v-on:click="location.href='/vistaConcurrentes/'+conc.dni"> Detalles </button>
        <button class="btn btn-danger" v-on:click="eliminarConcurrente(conc.dni)"> Eliminar </button>
      </td>
    </tr>
  </tbody>
</table>


</div> <!-- Fin container -->
</body>

<script type="text/javascript" src="/js/vue.min.js"></script>
<script type="text/javascript" src="/js/vue-resource.min.js"></script>

<script type="text/javascript" src="/js/app_concurrentes.js"></script>

@endsection